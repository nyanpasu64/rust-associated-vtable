# Rust - Storing trait "associated statics" in trait-object vtables

Currently, if you try to use a trait with an associated `const` as a trait object, you encounter E0038.

https://doc.rust-lang.org/stable/error-index.html#the-trait-cannot-contain-associated-constants

> Just like static functions, associated constants aren't stored on the method table. If the trait or any subtrait contain an associated constant, they cannot be made into an object.
> 
> ```rust
> trait Foo {
>     const X: i32;
> }
> 
> impl Foo {}
> ```
> 
> A simple workaround is to use a helper method instead:
> 
> ```rust
> trait Foo {
>     fn x(&self) -> i32;
> }
> ```

What if it was possible to store associated statics on the method table? Various names and synonyms:

- dyn(amic) trait constants
- virtual static variables
- associated constants in trait objects
- abstract ClassVar (in Python)

<!-- Neither C++ nor Rust has the ability to store static constants directly in vtables. It would be syntactically simpler, as well as possibly faster, than virtual methods that ignore `this`.

link to my earlier message: https://discordapp.com/channels/273534239310479360/274215136414400513/647047418705084417
 -->
 
## Advantages

- Reduced subclass boilerplate. Currently, you need a bit of boilerplate to define the function in the trait, and implement the function in each struct implementing the trait. Maybe the boilerplate is less tedious to type in Rust than in C++.
    - In my current project, I wrote a C++ macro to automate the boilerplate. I suppose you could write a Rust macro to automate the boilerplate too.
- No parentheses at the call site. Minor syntactic improvement (if you call it an improvement). It's more clearly side-effect free than a method call.
- Possibly a speed improvement if indirect lookups are faster than virtual method calls.

## Issues

I've been told that Rust `const` is similar to C++ `#define` (every usage hard-codes the value, so the `const` has no fixed address). <!-- This is conceptually incompatible with `dyn trait` static variables (where each trait-impl's method pointers and associated statics have fixed locations). --> So associated statics would need to be static variables instead.

The issue is that in Rust (unlike Microsoft's unreleased Midori language), regular static variables can have interior mutability, and are not stored in read-only address space. I assume method tables are stored in read-only address space. If associated statics were stored there too, interior-mutable operations (like pushing/popping from a `&crossbeam::queue`) would crash at runtime (I think).

Link to Midori blog post: [Joe Duffy - Safe Native Code](http://joeduffyblog.com/2015/12/19/safe-native-code/)

> I mentioned offhandedly earlier that Midori had no mutable statics. More accurately, we extended the notion of `const` to cover any kind of object. This meant that static values were evaluated at compile-time, written to the readonly segment of the resulting binary image, and shared across all processes. More importantly for code quality, all runtime initialization checks were removed, and all static accesses simply replaced with a constant address.

My intention is to ban types with interior mutability from "associated statics/consts".

matt1992:
_In rust there is the std private Frozen trait for that
Types that can be stored in the program binary(or other forms of read-only memory),because they don’t contain internal mutability._

I was unable to find the Frozen trait. Maybe I didn’t look in the right place. Looking through [https://github.com/rust-lang/rust](https://github.com/rust-lang/rust), I was only able to find PointerKind::Frozen at [src/librustc/ty/layout.rs](https://github.com/rust-lang/rust/blob/e862c01aadb2d029864f7bb256cf6c85bbb5d7e4/src/librustc/ty/layout.rs).


## Use cases, motivation, importance (subclass system vs. trait system)

The motivation behind this is metadata/introspection supported by all instances of a base class or dyn trait, which varies by which subclass or struct implements that trait, but not by what the subclass/struct contains.

There are two possible usage locations for this feature: Accessing "virtual statics" within trait methods, or accessing "virtual statics" from outside code.

### Virtual statics used by trait methods

```rust
trait Number {
    fn num(&self) -> i32;
    fn print_num(&self) {
        println!("{}", self.num());
    }
}

struct S {}
impl Number for S {
    fn num(&self) -> i32 {
        1
    }
}

fn main() {
    S {}.print_num();
}
```

I think trait-object default-methods are kinda crippled compared to C++ base-class methods (which can access fields through the head of `*this`), or even Java interface default-methods since any getter/setter of `self` borrows the entire `self`. I think there were discussions on Rust "base classes or traits with data" already.

<!-- In Rust, trait default methods that can't store data (through the head of `*self`) are less useful than C++ base classes, and closer to Java interfaces. And even more crippled, -->

### Virtual statics used by outside code

For the case of accessing "virtual statics" from outside code (often a separate code module), I think this proposal is just as useful as in C++. Accessing "struct with impl" / "pointer to base class" fields from outside the impl is not possible in Rust, but I feel it's already uncommon in many languages (maybe even an antipattern?).

## Examples in existing code

I have many examples of this pattern being used in my own code and others' code.

Unfortunately, some of the usage is in trait methods (C++ base-class methods), which are crippled in Rust compared to C++. So this feature may be less useful in Rust.

### Qt library, QMetaObject

Qt has a single getter pointing to a giant pile of metadata: https://doc.qt.io/qt-5/qmetaobject.html

> A single QMetaObject instance is created for each QObject subclass that is used in an application, and this instance stores all the meta-information for the QObject subclass.
 
One instance of this metadata exists for each QObject subclass. To obtain a pointer, call [QObject::metaObject()](https://doc.qt.io/qt-5/qobject.html#metaObject) on an instance.

This approach avoids the issue where program will crash if any user of the static metadata modifies it. However it may be slower, since it requires one virtual function call to obtain a `QMetaObject*`:

- dereferencing the pointer to QObject and indexing the pointer to vtable
- dereferencing QObject's pointer to vtable and indexing `QObject::metaObject()`
- calling the method to obtain `QMetaObject*`
- once per metadata item, dereferencing to obtain QMetaObject (same `QMetaObject` every time, high memory locality, each metadata getter is a nonvirtual method call)

I hope I got the sequence right. And in Rust, pointers are fat, so you don't need a double dereference?

### C++, my code (best concrete example in my opinion)

My most compelling use case so far is some real/production C++ code. A central function loops over a series of emulated sound channels, and asks each channel for its highest valid waveform type. Then on the GUI, my program marks any invalid (too-large) waveform types as red.

Associated consts/statics is mostly "slightly nicer" than virtual functions (less boilerplate for the programmer to type/read, slightly faster to run), and "takes less memory, less confusing, and is Rust-compatible" unlike storing these subclass-specific parameters as base-class instance data.

This feature is not a "life or death" situation, nor enables anything previously impossible.

To explore the problem space, I have approximately translated this part of the program to Rust, using associated constants (which do *not* work with dyn Trait at the moment) rather than virtual methods. [https://play.rust-lang.org/...](https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=02e853dffd8f66e338cea7d75bc35c6e)

https://github.com/jimbo1qaz/j0CC-FamiTracker/blob/c17ba0d44ed6102970c03c2948b8a526bcbebf89/Source/ChannelHandler.h#L331-L336

```cpp
class CChannelHandler : public CChannelHandlerInterface {
public:
	/*!
	 * Returns maximum valid duty cycle, inclusive (-1 if none are valid).
	 * Used to mark invalid Vxx red in the GUI.
	 * @return Valid duty cycles are 0 <= duty <= getDutyMax().
	 */
	virtual int getDutyMax() const;
```

getDutyMax() is called by outside code in a different module, on an *instance* (base-class pointer) of CChannelHandler. This pattern translates to Rust as-is, and would work equally in C++ and Rust.

In a different project, I later defined a C++ macro which automates the boilerplate of defining and implementing a virtual function ([gitlab.com/nyanpasu64/exotracker-cpp/...](https://gitlab.com/nyanpasu64/exotracker-cpp/blob/6f9437255f3a30d5811d27faa68df46bbad9d5dc/src/audio/synth_common.h#L134-139)), but I haven't actually called this function anywhere...

### C++, previous maintainer's code in the same project

https://github.com/jimbo1qaz/j0CC-FamiTracker/blob/c17ba0d44ed6102970c03c2948b8a526bcbebf89/Source/ChannelHandler.cpp#L48-L69

```cpp
CChannelHandler::CChannelHandler(int MaxPeriod, int MaxVolume) : 
    ...
	m_iMaxPeriod(MaxPeriod),
	m_iMaxVolume(MaxVolume),
    ...
{
}
```

https://github.com/jimbo1qaz/j0CC-FamiTracker/blob/c17ba0d44ed6102970c03c2948b8a526bcbebf89/Source/Channels2A03.cpp#L39-L40

```cpp
CChannelHandler2A03::CChannelHandler2A03() :
	CChannelHandler(0x7FF, 0x0F),
    ...
{
}
```

`m_iMaxPeriod` and `m_iMaxVolume` are initialized through the base-class constructor. However, these constructor arguments are not exposed to subclass constructors. Instead, each subclass constructor supplies the base-class constructor with a subclass-specific value.

These serve the same purpose as virtual (subclass-specific) static variables, but are actually stored as instance variables, and have a misleading-to-newcomers constructor implementation.

These variables are only accessed by base-class methods (which are severely limited in Rust).

### Python, my code

Turns out this is a bad example. My use case only involved statically resolving fields through a **subclass name**, not a pointer to an instance. In Rust, I think this is already possible through (struct/trait?) associated consts and static binding.

https://github.com/jimbo1qaz/corrscope/blob/2dec518878cb104744637f803f6de7c2aab8df31/corrscope/renderer.py#L232-L246

```python
@property
@abstractmethod
def abstract_classvar(self) -> Any:
    """A ClassVar to be overriden by a subclass."""

class _RendererBackend(ABC):
    """
    Renderer backend which takes data and produces images.
    """

    # Class attributes and methods
    bytes_per_pixel: int = abstract_classvar
    ffmpeg_pixel_format: str = abstract_classvar
    ...

class MatplotlibAggRenderer(_RendererBackend):
    # Implements _RendererBackend.
    bytes_per_pixel = 3
    ffmpeg_pixel_format = "rgb24"
    ...
```

I implemented this in Python (but in a base class, not a trait).

`bytes_per_pixel` and `ffmpeg_pixel_format` are used in outside code, but through a **subclass name** (`Renderer.bytes_per_pixel`), not through an instance of any particular subclass. In Rust, I think this is already possible through (struct/trait?) consts and static binding.

`bytes_per_pixel` is used by an an "intermediate class" method, but implemented in a leaf subclass. Neither deep subclass/inheritance hierarchies nor accessing data in a trait method would translate well to Rust.

<!-- In the base class, I marked each as an abstract property, or a virtual method called even if you don't supply parentheses. In the subclasses, I instead shadowed the *property* with a static *variable* (not property or method). A bit odd, but it worked (I don't know if the ABC metaclass complained that these methods weren't overriden, because it only detected override functions rather than variables.) -->

## Prior art

https://github.com/rust-lang/rust/issues/26847#issuecomment-298882382 2017
> this ICEs

> Associated consts are not object safe.

> Can't associated consts be part of the vtable/fat pointers?

> Associated consts are inlined at compiletime, they can't have a value which is determined at runtime. This is a hard requirement for making things like const fn and const generics work.
>
> Associated statics could be object safe if they existed.

## Feedback in the community Discord server

### 2019-11-21

https://discordapp.com/channels/273534239310479360/274215136414400513/647047418705084417

me:
idea: placing constants (not just function pointers) in method tables

Nemo157:
*trait-object constants seem pretty much identical to object-safe `self`-less functions
so I assume the same objections that it's weird to call a function on a receiver that's not used would apply*

me:
am i correct in thinking that calling a vtable method seems slower than just loading an int/&str from vtable?

Nemo157:
*nope, even if it's the same speed cause of optimizations (either llvm de-virtualizing or cpu caching) it's definitely doing more work*
*I agree with you that the method has to be slower*
*but if we get consts on trait objects I see no reason to not also have receiver-less functions there
not that they're a replacement, just that they're basically identically powerful and could be used to emulate each other*

me:
ohhh, actually, putting associated _consts_ into the vtable probably doesn't make sense since they're compile-time only, associated _statics_ would make more sense
like storing i32 in a vtable is definitely a good idea, &str? probably not some 20-element struct with fancy impl, let alone a crossbeam queue with interior mutability

### 2019-12-02

https://discordapp.com/channels/273534239310479360/274215136414400513/651307086940078100

me:
http://joeduffyblog.com/2015/12/19/safe-native-code/

> I mentioned offhandedly earlier that Midori had no mutable statics. More accurately, we extended the notion of const to cover any kind of object.

A while back I asked "what if we could store constants, not just methods, in dyn trait tables?"
Someone replied that that would be closer to static than const. But in Rust, statics can have interior mutability (notably, crossbeam queues are fully mutable through a shared & reference).
Is there a term for "statics which have no interior mutability and can be read-only and mapped directly from the on-disk .exe"?

matt1992:
*In rust there is the std private Frozen trait for that
Types that can be stored in the program binary(or other forms of read-only memory),because they don't contain internal mutability.*

*Ah,that would be more like associated statics.
I would be very interested in Rust getting immutable statics in the vtable.
And being able to get them in trait objects*

(I was unable to find the Frozen trait. Maybe I didn't look in the right place. Looking through https://github.com/rust-lang/rust, I was only able to find PointerKind::Frozen at [src/librustc/ty/layout.rs](https://github.com/rust-lang/rust/blob/e862c01aadb2d029864f7bb256cf6c85bbb5d7e4/src/librustc/ty/layout.rs).)
